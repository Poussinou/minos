/*
    Minos - a little maze game
 
    Copyright (C)2020 Torsten Crass
    
    https://gitlab.com/tcrass/minos
    me@tcrass.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

package de.tcrass.minos;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import de.tcrass.minos.controller.GameController;
import de.tcrass.minos.model.Game;
import de.tcrass.minos.model.GameState;
import de.tcrass.minos.model.SettingsFormData;
import de.tcrass.minos.model.factory.GameFactory;
import de.tcrass.minos.model.factory.GameMetrics;
import de.tcrass.minos.view.DisplayUtils;
import de.tcrass.minos.view.GameView;
import de.tcrass.minos.view.SettingsWindow;
import de.tcrass.minos.event.GameSettingsChangedEvent;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String STATE_KEY_GAME = "game";
    private static final String STATE_KEY_GAME_SETTINGS = "gameSettings";
    private static final String STATE_KEY_SCREEN_ORIENTATION = "screenOrientation";

    private FrameLayout gameContainer = null;
    private GameView gameView = null;
    private SettingsWindow settingsWindow = null;
    private Menu menu;

    private GameMetrics gameMetrics = null;

    private GameController gameController = null;

    private Animation destroyMazeAnimation = null;
    private Animation createMazeAnimation = null;

    /* --- Instance state --- */
    private Game game;
    private SettingsFormData gameSettings;
    private int screenOrientation;

    /* --- Live cycle methods ------------------------------------- */

    /*
     * Activity has been instantiated
     */
    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.activity_main);
        gameContainer = (FrameLayout) findViewById(R.id.gameContainer);

        // App has been started for the first time
        if (savedState == null) {

            // Determine and lock current screen orientation
            screenOrientation = DisplayUtils.getCurrentScreenOrientation(this);
            setRequestedOrientation(screenOrientation);

            gameSettings = new SettingsFormData();
        }

    }

    /*
     * Will be called after onCreate, but only if the Activity has been killed
     * since it was last visible
     */
    @Override
    public void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);
        Log.i(TAG, "onRestoreInstanceState");

        screenOrientation = savedState.getInt(STATE_KEY_SCREEN_ORIENTATION);
        if (screenOrientation == ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
            // Determine and lock current screen orientation
            screenOrientation = DisplayUtils.getCurrentScreenOrientation(this);
            setRequestedOrientation(screenOrientation);
        }

        gameSettings = savedState.getParcelable(STATE_KEY_GAME_SETTINGS);

        game = savedState.getParcelable(STATE_KEY_GAME);
        if (game != null) {
            gameContainer.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Creating game metrics & view...");
                    // Re-create game metrics...
                    gameMetrics = new GameMetrics(gameContainer, gameSettings);
                    // ...assign to game...
                    game.setMetrics(gameMetrics);
                    // ...and create game view
                    createGameView();
                }
            });
        }
    }

    /*
     * Instance state has been restored
     */
    @Override
    protected void onPostCreate(Bundle savedState) {
        super.onPostCreate(savedState);
        Log.i(TAG, "onPostCreate");
        // If this is the application's first launch or no game has been
        // persisted...
        if (game == null) {
            // ...request creation of a new game
            createGame();
        }

    }

    /*
     * Called if activity has previously been stopped, i.e. brought into the
     * background
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart");
    }

    /*
     * Activity becomes visible
     */
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    /*
     * Activity becomes interactive
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

        // Start listening to game settings changes
        EventBus.getDefault().register(this);
    }

    /* --- Activity is active --- */

    /*
     * Activity has stopped interacting
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");

        // Stop listening to game settings changes
        EventBus.getDefault().unregister(this);
    }

    /*
     * May be called before or after onPause (or not at all)
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i(TAG, "onSaveInstanceState");
        outState.putParcelable(STATE_KEY_GAME, game);
        outState.putParcelable(STATE_KEY_GAME_SETTINGS, gameSettings);
        outState.putInt(STATE_KEY_SCREEN_ORIENTATION, screenOrientation);
        super.onSaveInstanceState(outState);
    }

    /*
     * Activity has become invisible
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
        // If there is a game settings window...
        if (settingsWindow != null) {
            // ...close and forget
            settingsWindow.dismiss();
            settingsWindow = null;
        }
    }

    /*
     * Activity will be removed from memory?
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,
                "onDestroy: "
                        + Integer.toString(getChangingConfigurations(), 16));
    }

    /* --- Menu / action handling ---------------------------------- */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu");
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "onOptionsItemSelected: " + item.getTitle());

        switch (item.getItemId()) {
        case R.id.action_newGame:
            createGame();
            return true;
        case R.id.action_settings:
            showSettingsWindow();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateMenu() {
        if (settingsWindow != null) {
            menu.findItem(R.id.action_newGame).setEnabled(false);
            menu.findItem(R.id.action_newGame).getIcon().setAlpha(64);
            menu.findItem(R.id.action_settings).setEnabled(false);
            menu.findItem(R.id.action_settings).getIcon().setAlpha(64);
        } else {
            menu.findItem(R.id.action_newGame).setEnabled(true);
            menu.findItem(R.id.action_newGame).getIcon().setAlpha(255);
            menu.findItem(R.id.action_settings).setEnabled(true);
            menu.findItem(R.id.action_settings).getIcon().setAlpha(255);
        }
    }

    /* --- UI de.tcrass.minos.event handlers --------------------------------------- */

    private void showSettingsWindow() {
        Log.i(TAG, "showSettingsWindow");
        // If not already visible...
        if (settingsWindow == null) {
            // Pause game and remember old game state
            final GameState oldGameState = game.getState();
            gameController.setGameState(GameState.PAUSED);
            // Create settings window
            View content = getLayoutInflater().inflate(
                    R.layout.settings_window, null);
            View anchor = findViewById(R.id.action_settings);
            settingsWindow = new SettingsWindow(content, gameSettings);
            // Wire up settings window's OK button
            Button okButton = (Button) content.findViewById(R.id.okButton);
            okButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Since we're working directly with the surrounding's
                    // instance's
                    // member rather than a final variable, make sure that
                    // there really is a settings window instance.
                    if (settingsWindow != null) {
                        // Close and forget
                        settingsWindow.dismiss();
                        settingsWindow = null;
                        updateMenu();
                        // New game created?
                        gameView.setInBackground(false);
                        if (game.getState() == GameState.READY) {
                            gameController.setGameState(GameState.PLAYING);
                        } else {
                            gameController.setGameState(oldGameState);
                        }
                    }
                }
            });
            // Place overlay over game
            gameView.setInBackground(true);
            // Show the settings window
            settingsWindow.showAsDropDown(anchor);
            // Will disable all action bar items
            updateMenu();
        }
    }

    /* --- Game handling ------------------------------------------- */

    private void createGame() {
        Log.i(TAG, "createGame");

        // Forget the old game (if any)
        if (gameController != null) {
            gameController.setGameState(GameState.PAUSED);
        }
        game = null;

        // Store current screen orientation
        int currentScreenOrientation = DisplayUtils
                .getCurrentScreenOrientation(this);

        // Only allow screen re-orientation if settings window is not showing
        if (settingsWindow == null) {
            // Grant App a chance to re-orient
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
            // Store and lock new screen orientation
            screenOrientation = DisplayUtils.getCurrentScreenOrientation(this);
            setRequestedOrientation(screenOrientation);
        }

        // If screen orientation has not changed...
        if (screenOrientation == currentScreenOrientation) {
            gameContainer.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Creating game...");
                    // Determine game metrics
                    gameMetrics = new GameMetrics(gameContainer, gameSettings);
                    // Create game
                    game = GameFactory.createGame(gameMetrics);
                    // Create game view
                    createGameView();
                    gameController = new GameController(MainActivity.this,
                            game, gameView);
                    gameView.setGameController(gameController);
                    if (settingsWindow == null) {
                        gameController.setGameState(GameState.PLAYING);
                    } else {
                        gameView.setInBackground(true);
                    }
                }
            });

        } else {
            // Game will be created during activity re-creation
        }

    }

    private void createGameView() {
        Log.i(TAG, "createGameView");
        // Remove old game view
        if (gameView != null) {
            final ViewGroup gc = gameContainer;
            final View gv = gameView;
            if (createMazeAnimation != null && !createMazeAnimation.hasEnded()) {
                Log.d(TAG, "stopping createMazeAnimation");
                createMazeAnimation.cancel();
            }
            destroyMazeAnimation = AnimationUtils.loadAnimation(this,
                    R.anim.destroy_maze);
            Log.d(TAG, "starting destroyMazeAnimation");
            destroyMazeAnimation.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    gc.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "destroyMazeAnimation finished, removing old game view");
                            gc.removeView(gv);
                        }
                    });
                }
            });
            gameView.startAnimation(destroyMazeAnimation);
        }
        // Create and add new game view
        gameView = new GameView(this, game);
        // gameView.setVisibility(View.GONE);
        gameContainer.addView(gameView, 0);
        // if (createMazeAnimation != null && !createMazeAnimation.hasEnded()) {
        // Log.d(TAG, "stopping createMazeAnimation");
        // createMazeAnimation.cancel();
        // }
        Log.d(TAG, "starting createMazeAnimation");
        createMazeAnimation = AnimationUtils.loadAnimation(this,
                R.anim.create_maze);
        gameView.startAnimation(createMazeAnimation);
    }

    /* --- Game settings handling --- */

    @Subscribe
    public void onGameSettingsChanged(GameSettingsChangedEvent event) {
        Log.i(TAG, "onGameSettingsChanged");
        // Only create new game if metrics have changed
        GameMetrics newGameMetrics = new GameMetrics(gameContainer,
                gameSettings);
        if (!newGameMetrics.equals(gameMetrics)) {
            createGame();
        }
    }

}
